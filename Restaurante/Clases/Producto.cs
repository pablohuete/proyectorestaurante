﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurante.Clases
{
    public class Producto
    {
        public int Id { get; set; }
        public String Descripcion { get; set; }
        public Double PrecioBase { get; set; }
        public Double PorcentajeImpuesto { get; set; }
        public bool esPlatoFuerte { get; set; }
        public double PorcentajeDescuento { get; set; }

        public Producto(int id, string descripcion, double precioBase, double porcentajeImpuesto, bool esPlatoFuerte, double porcentajeDescuento)
        {
            Id = id;
            Descripcion = descripcion;
            PrecioBase = precioBase;
            PorcentajeImpuesto = porcentajeImpuesto;
            this.esPlatoFuerte = esPlatoFuerte;
            PorcentajeDescuento = porcentajeDescuento;
        }
        public Double ObtenerPrecioFinal()
        {
            return (PrecioBase - ObtenerDescuento())+
                this.ObtenerImpuesto();
        }

        public Double ObtenerImpuesto()
        {
            return (this.PrecioBase - this.ObtenerDescuento()) *
                 (this.PorcentajeImpuesto / 100f);
        }
        public Double ObtenerDescuento()
        {
            return this.PrecioBase * (this.PorcentajeDescuento / 100f);
        }
        public override string ToString()
        {
            return "Producto:" + this.Descripcion +
                ",Precio:" + this.ObtenerPrecioFinal().ToString() +
                "\n";
        }
    }
 }

