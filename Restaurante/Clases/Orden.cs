﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Restaurante.Clases;

namespace Restaurante.Clases
{
    public class Orden
    {
        public static int contador = 0;
        public string Cliente { get; set; }
        public List<Producto> Productos;
        private int numero;
        public double PorcentajeDescuentoVigente;

        public Orden(string pCliente, double pPorcentajeDescVigente)
        {
            contador++;
            this.Cliente = pCliente;
            this.Productos = new List<Producto>();
            this.numero = contador;
            this.PorcentajeDescuentoVigente = pPorcentajeDescVigente;
        }

        public int Numero {
            get { return this.numero; }
        }

        private bool EsCombo()
        {
            bool esCombo = false;
            int acompañamientos = 0;
            int platosF = 0;
            foreach (Producto aux in this.Productos)
            {
                if (aux.esPlatoFuerte)
                {
                    platosF++;
                }
                else
                {
                    acompañamientos++;
                }
            }
            if ((platosF >= 1) && (acompañamientos >= 2))
                esCombo = true;
            return esCombo;
            /*
            var platoF = from x in this.Productos
                         where x.esPlatoFuerte = true
                         select x;

            var acompanamientos = from x in this.Productos
                                  where x.esPlatoFuerte = false
                                  select x;
            if ((platoF.Count() >= 1) && (acompanamientos.Count() >= 2))
                esCombo = true;
            */
        }

        public double ObtenerSubtotal()
        {
            double subtotal = 0;
            foreach (Producto p in this.Productos)
            {
                subtotal += p.PrecioBase;
            }
            return subtotal;
        }
        private void ValidarCombo()
        {
            if (this.EsCombo())
            {
                this.Productos.ForEach(x => x.PorcentajeDescuento = this.PorcentajeDescuentoVigente);
            }
            else
            {
                this.Productos.ForEach(x => x.PorcentajeDescuento = 0);
            }
            
        }
        public double ObtenerTotalDescuentos()
        {
            ValidarCombo();
            //var sum = this.Productos.Select(p => p.ObtenerDescuento()).Sum();
            double sum = 0;
            foreach (Producto valor in this.Productos)
            {
                sum += valor.ObtenerDescuento();
            }
            return sum;
        }

        public double ObtenerTotalImpuestos()
        {
            double totalImpuestos = 0;
            foreach (Producto p in this.Productos)
            {
                totalImpuestos += p.ObtenerImpuesto();
            }
            return totalImpuestos;
        }

        public double ObtenerTotal()
        {
            return (this.ObtenerSubtotal() - this.ObtenerTotalDescuentos()) + this.ObtenerTotalImpuestos();
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("Orden:" + this.Numero.ToString());
            str.AppendLine("Cliente:" + this.Cliente);
            str.AppendLine("***********************************************");
            foreach (Producto p in this.Productos)
            {
                str.AppendLine(p.ToString());
            }
            str.AppendLine("***********************************************");
            str.AppendLine("Subtotal:" + this.ObtenerSubtotal().ToString());
            str.AppendLine("Descuentos:" + this.ObtenerTotalDescuentos().ToString());
            str.AppendLine("Impuesto Total:" + this.ObtenerTotalImpuestos().ToString());
            str.AppendLine("Total:" + this.ObtenerTotal().ToString());
            str.AppendLine("***********************************************");
            return str.ToString();

        }
    }
}
