﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Restaurante.Repositorios.Datos;

namespace Restaurante.Repositorios
{
    public class Producto
    {
        /// <summary>
        /// Consulta a la base de datos en la tabla producto
        /// </summary>
        /// <returns>Arreglo de productos</returns>
        public List<Clases.Producto> ObtenerProductos()
        {
            List<Clases.Producto> productos = new List<Clases.Producto>();
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                DataSet dataSet = cnx.ejecutarConsultaSQL("select p.*," +
                "CASE WHEN paga_impuesto = 'S' THEN pt.porcentaje_impuesto_vigente " +
                "ELSE 0 END as porcentaje_impuesto " +
                "from " + cnx.Schema + ".producto p, " + cnx.Schema + ".parametro pt");

                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        int id = Int32.Parse(fila["id"].ToString());
                        string descripcion = fila["descripcion"].ToString();
                        double precioBase = Double.Parse(fila["precio_base"].ToString());
                        double porcentaImpuesto = Double.Parse(fila["porcentaje_impuesto"].ToString());
                        bool esPlatoFuerte = fila["es_plato_fuerte"].ToString() == "S";

                        productos.Add(new Clases.Producto(id, descripcion, precioBase, porcentaImpuesto, esPlatoFuerte, 0));
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return productos;
        }
    }
}
