﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Restaurante.Repositorios.Datos;

namespace Restaurante.Repositorios
{
    public class Parametros
    {
        /// <summary>
        /// Método de consulta a la base de datos que obtiene el impuesto de la base de datos
        /// </summary>
        /// <returns>Porcentaje de impuesto vigente</returns>
        public double ObtenerImpuestoVigente()
        {
            double imp = 0;
            try
            {
                AccesoDatosPostgre conexion = AccesoDatosPostgre.Instance;
                string sql = "SELECT porcentaje_impuesto_vigente AS porcentaje_impuesto" +
                    " FROM " + conexion.Schema + ".parametro;";
                DataSet dataSet = conexion.ejecutarConsultaSQL(sql);

                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        imp = Double.Parse(fila["porcentaje_impuesto"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return imp;
        }

        /// <summary>
        /// Método de consulta a la base de datos que obtiene el descuento de la base de datos
        /// </summary>
        /// <returns>Porcentaje de descuento vigente</returns>
        public double ObtenerDescuentoVigente()
        {
            double imp = 0;
            try
            {
                AccesoDatosPostgre conexion = AccesoDatosPostgre.Instance;
                string sql = "SELECT porcentaje_descuento_vigente AS porcentaje_descuento" +
                    " FROM " + conexion.Schema + ".parametro;";
                DataSet dataSet = conexion.ejecutarConsultaSQL(sql);

                if (dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        imp = Double.Parse(fila["porcentaje_descuento"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return imp;
        }
    }
}
