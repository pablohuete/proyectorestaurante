﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Restaurante.Repositorios.Datos;

namespace Restaurante.Repositorios
{
    public class Orden
    {
        /// <summary>
        /// Inserta una nueva orden en la base de datos y llama a AgregarDetalleOrden
        /// </summary>
        /// <param name="pOrden">Orden a insertar</param>
        /// <returns>Id de la orden agregada en la BD</returns>
        public int AgregarOrden(Clases.Orden pOrden)
        {
            int id = 0;
            string strId = "";
            AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
            try
            {
                cnx.iniciarTransaccion();
                string sql = "INSERT INTO public.orden" +
                             "(fecha_hora, cliente, porcentaje_descuento) " +
                             "VALUES(@fecha_hora, @cliente, @porcentaje_descuento) returning id";
                Parametro oParametro = new Parametro();

                oParametro.agregarParametro("@fecha_hora", NpgsqlTypes.NpgsqlDbType.Timestamp, DateTime.Now);
                oParametro.agregarParametro("@cliente", NpgsqlTypes.NpgsqlDbType.Varchar, pOrden.Cliente);
                oParametro.agregarParametro("@porcentaje_descuento", NpgsqlTypes.NpgsqlDbType.Double, pOrden.PorcentajeDescuentoVigente);

                cnx.ejecutarSQL(sql, oParametro.obtenerParametros(), ref strId);
                id = Int32.Parse(strId);

                int linea = 1;
                foreach (Clases.Producto _Producto in pOrden.Productos)
                {
                    //Llamar a metodo insertar linea de detalle
                    this.AgregarDetalleOrden(id, linea, _Producto);
                    linea++;
                }
                cnx.commitTransaccion();
            }
            catch (Exception ex)
            {
                cnx.rollbackTransaccion();
                throw ex;
            }
            return id;
        }
        /// <summary>
        /// Método privado con el cual se inserta en la tabla detalle_orden en la BD
        /// </summary>
        /// <param name="pOrden">Nueva orden</param>
        /// <param name="pLinea">Número del detalle</param>
        /// <param name="pProducto">Producto conetenido en la orden</param>
        private void AgregarDetalleOrden(int pOrden, int pLinea, Clases.Producto pProducto)
        {
            try
            {
                AccesoDatosPostgre cnx = AccesoDatosPostgre.Instance;
                string sql = "INSERT INTO " + cnx.Schema +
                    ".detalle_orden(numero_linea, orden, producto, descripcion, precio_base, " +
                    "porcentaje_impuesto_vigente, es_plato_fuerte) " +
                    "VALUES(@numero_linea, @orden, @producto, @descripcion, @precio_base, " +
                    "@porcentaje_impuesto, @es_plato_fuerte);";

                Parametro parametro = new Parametro();
                parametro.agregarParametro("@numero_linea", NpgsqlTypes.NpgsqlDbType.Integer, pLinea);
                parametro.agregarParametro("@orden", NpgsqlTypes.NpgsqlDbType.Integer, pOrden);
                parametro.agregarParametro("@producto", NpgsqlTypes.NpgsqlDbType.Integer, pProducto.Id);
                parametro.agregarParametro("@descripcion", NpgsqlTypes.NpgsqlDbType.Varchar, pProducto.Descripcion);
                parametro.agregarParametro("@precio_base", NpgsqlTypes.NpgsqlDbType.Double, pProducto.PrecioBase);
                parametro.agregarParametro("@porcentaje_impuesto", NpgsqlTypes.NpgsqlDbType.Double, pProducto.PorcentajeImpuesto);
                parametro.agregarParametro("@es_plato_fuerte", NpgsqlTypes.NpgsqlDbType.Varchar, (pProducto.esPlatoFuerte ? "S" : "N"));

                cnx.ejecutarSQL(sql, parametro.obtenerParametros());
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //public Lis
    }
}
