﻿using Restaurante.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurante.Archivos
{
    public static class Parametros
    {

        public static double ObtenerImpuestoVigente()
        {
            double imp = 0;
            try
            {
                string ruta = AppDomain.CurrentDomain.BaseDirectory + "\\parametros.txt";
                ArchivoTexto arch = new ArchivoTexto(ruta);
                string error = "";
                List<string> lista = arch.leer_lista(ref error);
                imp = Double.Parse(lista[0]);
            }
            catch (Exception ex)
            {

                imp = 0;
                Console.WriteLine(ex.Message);
            }
            return imp;
        }

        public static double ObtenerPorcentajeDescuentoVigente()
        {
            double desc = 0;
            try
            {
                string ruta = AppDomain.CurrentDomain.BaseDirectory + "parametros.txt";
                ArchivoTexto arch = new ArchivoTexto(ruta);
                string error = "";
                List<string> lista = arch.leer_lista(ref error);
                desc = Double.Parse(lista[1]);
            }
            catch (Exception ex)
            {
                desc = 0;
                Console.WriteLine(ex.Message);
            }
            return desc;
        }

        public static List<Producto> ObtenerProductos()
        {
            double imp = ObtenerImpuestoVigente();
            int id = 0;
            List<Producto> productos = new List<Producto>();
            try
            {
                string ruta = AppDomain.CurrentDomain.BaseDirectory + "productos.txt";
                ArchivoTexto arch = new ArchivoTexto(ruta);
                string error = "";
                List<string> lista = arch.leer_lista(ref error);
                foreach (string linea in lista)
                {
                    string[] lineaSeparada = linea.Split(',');
                    bool esPlatof = false;
                    if (lineaSeparada[3] == "S")
                        esPlatof = true;
                    double impuesto = 0;
                    if (lineaSeparada[2] == "S")
                        impuesto = imp;

                    Producto producto = new Producto(id, lineaSeparada[0], Double.Parse(lineaSeparada[1]), impuesto, esPlatof, 0);
                    productos.Add(producto);
                    id++;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return productos;
            
        }
        
    }
}
