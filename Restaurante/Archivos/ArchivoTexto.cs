﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Restaurante.Archivos
{
    public class ArchivoTexto : Archivo
    {
        public ArchivoTexto(string pRuta) : base(pRuta)
        {
        }

        public override string leer(ref string pError)
        {
            pError = "";
            string lineas = "";
            try
            {
                FileStream stream = new FileStream(this.Ruta, FileMode.Open, FileAccess.Read);
                StreamReader reader = new StreamReader(stream);
                while (reader.Peek() > -1) lineas += reader.ReadLine() + "\n";
                reader.Close();
                stream.Close();
            }
            catch (Exception ex)
            {
                pError = "Ha ocurrido un error" +
                         "\nDetalle técnico:" +
                         ex.Message;
            }
            return lineas;
        }

        public List<string> leer_lista(ref string pError)
        {
            pError = "";
            List<string> lineas = new List<string>();
            try
            {
                FileStream stream = new FileStream(this.Ruta, FileMode.Open, FileAccess.Read);
                StreamReader reader = new StreamReader(stream);
                while (reader.Peek() > -1)
                    lineas.Add(reader.ReadLine());
                reader.Close();
                stream.Close();
            }
            catch (Exception ex)
            {
                pError = "Ha ocurrido un error" + ex.Message;
            }
            return lineas;

        }
        
        

        public override string escribir(string pTexto)
        {
            string error = "";
            try
            {
                StreamWriter writer = File.AppendText(this.Ruta);                
                writer.WriteLine(pTexto);
                writer.Close();
            }
            catch (Exception ex)
            {
                error = "Ha ocurrido un error" +
                         "\nDetalle técnico:" +
                         ex.Message;
            }
            return error;
        }

        public override string crear()
        { 
            string error = "";
            try
            {
                StreamWriter writer = File.CreateText(this.Ruta);
                writer.Close();
            }
            catch (Exception ex)
            {
                error = "Ha ocurrido un error" +
                         "\nDetalle técnico:" +
                         ex.Message;
            }
            return error;
        } 
      
    }
}
