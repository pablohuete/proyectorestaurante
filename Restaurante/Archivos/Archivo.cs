﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Restaurante.Clases;

namespace Restaurante.Archivos
{
    public abstract class Archivo
    {
        private string ruta;

        /// <summary>
        /// Almacena la ruta física en disco donde se 
        /// almacena el archivo
        /// </summary>
        protected string Ruta
        {
            get { return ruta; }            
        }

        /// <summary>
        /// Constructor de la clase Archivo
        /// </summary>
        /// <param name="pRuta">Define la ubicación física del archivo</param>
        public Archivo(string pRuta)
        {
            this.ruta = pRuta;
        }

        /// <summary>
        /// Abre el archivo y lo retorna en formato de 
        /// texto para visualizar su contenido
        /// </summary>
        /// <param name="pError">Variable por referencia, si no se retorna en blanco quiere decir que hubo error, el texto a su vez es el mensaje de error</param>
        /// <returns>Retorna un string con el contenido del archivo</returns>
        public abstract string leer(ref string pError);

        /// <summary>
        /// Toma un texto y lo añade al final del archivo
        /// </summary>
        /// <param name="pTexto">Texto que se desea agregar al archivo</param>
        /// <returns>un string que si no se retorna en blanco quiere decir que hubo error, el texto a su vez es el mensaje de error</returns>
        public abstract string escribir(string pTexto);

        /// <summary>
        /// Crea un nuevo archivo en la ruta especificada en el atributo
        /// ruta
        /// </summary>
        /// <returns>un string que si no se retorna en blanco quiere decir que hubo error, el texto a su vez es el mensaje de error</returns>
        public abstract string crear();

        /// <summary>
        /// Borra el archivo especificado en el atributo ruta
        /// </summary>
        /// <returns>un string que si no se retorna en blanco quiere decir que hubo error, el texto a su vez es el mensaje de error</returns>
        public virtual string borrar()
        {
            string error = "";
            try
            {
                File.Delete(this.Ruta);
            }
            catch (Exception ex)
            {
                error = "Ha ocurrido un error" +
                         "\nDetalle técnico:" +
                         ex.Message;
            }
            return error;
        }
    }
}
