﻿using Restaurante.Archivos;
using Restaurante.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurante
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Menu
            List<Orden> ordenes = new List<Orden>();
            bool again = true;
            do
            {
                Console.Clear();
                Console.Write("Parque de Diversiones\n" +
                "********************Menu********************\n" +
                "1. Agregar orden.\n" +
                "2. Consulta histórica de órdenes.\n" +
                "3. Salir!\n");

                int op = ReadInt("Digite una opción: ");
                switch (op)
                {
                    case 1:
                        Orden nuevaOrden = CapturarOrden();
                        ordenes.Add(nuevaOrden);
                        Console.WriteLine("Enter para volver al Menú...");
                        Console.ReadLine();
                        break;

                    case 2:
                        ObtenerConsultaOrdenes(ordenes);
                        Console.WriteLine("Enter para volver al Menú...");
                        Console.ReadLine();
                        break;
                }

                Console.WriteLine("Digite para " + (op == 3 ? "Salir" : "Continuar"));
                again = (op == 3 ? false : true);
                Console.ReadLine();
            }
            while (again);
            #endregion
        }

        /// <summary>
        /// Captura un valor de tipo Int por consola
        /// </summary>
        /// <param name="text">Texto para mostrar al usuario</param>
        /// <returns>Valor entero capturado</returns>
        internal static int ReadInt(string text = "Digite un numero entero")
        {
            bool result = false;
        START:
            Console.WriteLine(text);

            string data = Console.ReadLine();
            result = Int32.TryParse(data, out int value);
            if (!result)
            {
                Console.WriteLine("Intente de nuevo");
                goto START;
            }
            return value;
        }

        /// <summary>
        /// Imprime en Consola los detalles de las órdenes
        /// </summary>
        /// <param name="pOrdenes">Lista de órdenes</param>
        internal static void ObtenerConsultaOrdenes(List<Orden> pOrdenes)
        {
            double subtotalGeneral = 0;
            double descuentoGeneral = 0;
            double impuestosGeneral = 0;
            double totalGeneral = 0;
            Console.WriteLine("Listado de Ordenes");
            Console.WriteLine("********************************");
            foreach (Orden orden in pOrdenes)
            {
                Console.WriteLine(orden.ToString());
                subtotalGeneral += orden.ObtenerSubtotal();
                descuentoGeneral += orden.ObtenerTotalDescuentos();
                impuestosGeneral += orden.ObtenerTotalImpuestos();
                totalGeneral += orden.ObtenerTotal();
            }

            Console.WriteLine("*********************************");
            Console.WriteLine("Cantidad total de las órdenes: " + pOrdenes.Count.ToString());
            Console.WriteLine("Subtotal de las órdenes: " + subtotalGeneral.ToString());
            Console.WriteLine("Descuento de las órdenes: " + descuentoGeneral.ToString());
            Console.WriteLine("Impuestos de las órdenes: " + impuestosGeneral.ToString());
            Console.WriteLine("Total de las órdenes: " + totalGeneral.ToString());
            Console.WriteLine("*********************************");
        }

        internal static Orden CapturarOrden()
        {
            Console.WriteLine("Digite el nombre del cliente: ");
            string cliente = Console.ReadLine();
            double porcDesc = Parametros.ObtenerPorcentajeDescuentoVigente();
            Orden orden = new Orden(cliente, porcDesc);
            bool continuar = true;
            do
            {
                Producto producto = CapturarProducto();
                orden.Productos.Add(producto);
                Console.WriteLine("Producto agregado!");
                Console.WriteLine("Desea agregar otro? (N = no agregar más): ");
                string otro = Console.ReadLine().ToUpper();
                if (otro == "N")
                    continuar = false;
            } while (continuar);
            return orden;
        }

        internal static Producto CapturarProducto()
        {
            List<Producto> productos = Parametros.ObtenerProductos();
            Producto producto = null;
            bool continuar = true;
            int opcion = 0;
            do
            {
                int i = 1;
                foreach (Producto p in productos)
                {
                    Console.WriteLine(i.ToString() + ". " + p.ToString());
                    i++;
                }
                opcion = ReadInt("Producto seleccionado: ");
                if ((opcion > 0) && (opcion <= productos.Count()))
                {
                    continuar = false;
                    producto = productos[opcion - 1];
                }
                else
                {
                    Console.WriteLine("Opción incorrecta...");
                }
            } while (continuar);
            return producto;
        }
    }
}
