﻿using Restaurante.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App_Restaurante_Forms
{
    public partial class frmRestaurante : Form
    {
        public List<Orden> Ordenes { get; set; }
        public frmRestaurante()
        {
            InitializeComponent();
            this.Ordenes = new List<Orden>();
        }
        private void tsBtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                frmOrden frmNuevaOrden = new frmOrden();
                frmNuevaOrden.ShowDialog();
                if (frmNuevaOrden.Aceptar)
                {
                    int idNuevaOrden = 0;
                    Orden nuevaOrden = frmNuevaOrden.ObtenerOrden();
                    this.Ordenes.Add(nuevaOrden);
                    Restaurante.Repositorios.Orden _orden = new Restaurante.Repositorios.Orden();
                    idNuevaOrden = _orden.AgregarOrden(nuevaOrden);
                    MessageBox.Show("Orden número: " + idNuevaOrden.ToString() + " Agregada",
                        "Orden",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void tsBtnConsultar_Click(object sender, EventArgs e)
        {
            StringBuilder textoConsulta = new StringBuilder();
            double subtotalGeneral = 0;
            double descuentoGeneral = 0;
            double impuestosGeneral = 0;
            double totalGeneral = 0;

            textoConsulta.AppendLine(("Listado de Ordenes"));
            textoConsulta.AppendLine("***********************************************");
            foreach (Orden orden in this.Ordenes)
            {
                textoConsulta.AppendLine(orden.ToString());
                subtotalGeneral += orden.ObtenerSubtotal();
                descuentoGeneral += orden.ObtenerTotalDescuentos();
                impuestosGeneral += orden.ObtenerTotalImpuestos();
                totalGeneral += orden.ObtenerTotal();
            }

            textoConsulta.AppendLine("***********************************************");
            textoConsulta.AppendLine("Cantidad total de las órdenes: " + this.Ordenes.Count.ToString());
            textoConsulta.AppendLine("Subtotal de las órdenes: " + subtotalGeneral.ToString());
            textoConsulta.AppendLine("Descuento de las órdenes: " + descuentoGeneral.ToString());
            textoConsulta.AppendLine("Impuestos de las órdenes: " + impuestosGeneral.ToString());
            textoConsulta.AppendLine("Total de las órdenes: " + totalGeneral.ToString());
            textoConsulta.AppendLine("***********************************************");

            this.txtCierre.Text = "";
            this.txtCierre.Text = textoConsulta.ToString();
        }
    }
}
