﻿using Restaurante.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace App_Restaurante_Forms
{
    public partial class frmOrden : Form
    {
        public List<Producto> productosOrden;
        public bool Aceptar { get; set; }
        public frmOrden()
        {
            InitializeComponent();
            this.CargarProductos();
            this.productosOrden = new List<Producto>();
            this.Aceptar = false;
        }

        public void CargarProductos()
        {
            List<Producto> productos = new List<Producto>();
            Restaurante.Repositorios.Producto bDProducto = new Restaurante.Repositorios.Producto();
            try
            {
                productos = bDProducto.ObtenerProductos();
                foreach (var p in productos)
                {
                    this.cmbProductos.Items.Add(p);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
            }            
            /*
            List<Producto> productos = Parametros.ObtenerProductos();
            foreach (Producto prod in productos)
            {
                this.cmbProductos.Items.Add(prod);
            }
            */
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (this.cmbProductos.SelectedIndex != -1)
            {
                Producto oProductoSeleccionado = 
                    (Producto)this.cmbProductos.SelectedItem;
                this.productosOrden.Add(oProductoSeleccionado);
                this.grdProductos.DataSource = null;
                this.grdProductos.DataSource = productosOrden;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if ((this.txtCliente.Text != "") && (this.productosOrden.Count() > 0))
            {
                this.Aceptar = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Debe agregar el nombre del cliente y al menos un productos",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Devuelve la orden capturada desde la interfaz
        /// </summary>
        /// <returns></returns>
        public Orden ObtenerOrden()
        {
            Orden nuevaOrden = null;
            try
            {
                Restaurante.Repositorios.Parametros dBParametros =
                    new Restaurante.Repositorios.Parametros();
                nuevaOrden = new Orden(this.txtCliente.Text, dBParametros.ObtenerDescuentoVigente())
                {
                    Productos = this.productosOrden
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return nuevaOrden;
            /*
            Orden nuevaOrden = null;
            try
            {
                nuevaOrden = new Orden(this.txtCliente.Text,
                Parametros.ObtenerPorcentajeDescuentoVigente());
                nuevaOrden.Productos = this.productosOrden;
            }
            catch
            {
                nuevaOrden = null;
                
            }
            return nuevaOrden;
            */
        }
    }
}
